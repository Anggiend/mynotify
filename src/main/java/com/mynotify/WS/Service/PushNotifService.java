package com.mynotify.WS.Service;

import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;

import org.springframework.http.HttpEntity;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.client.RestTemplate;

import com.mynotify.WS.Model.Messages;

import org.json.JSONException;
import org.json.JSONObject;

public class PushNotifService {

	private static final String Firebase_server_key = "AAAAbroC_ds:APA91bGsNms8pECjNt0Mo-XeoM8VpymfpgKTsMno3tUcF8y1kg9m5I-7Vgh4MR95ae3wkDZH0TJu_9ru2_kFrgBRYYxBz-MF-b7cRquFmO5qe8OKVDV8hjz9Gfqqr48YbEJsvxFW7tl2";
	private static final String Firebase_API ="https://fcm.googleapis.com/fcm/send";		

	@Async
	public CompletableFuture<String> send(Messages messages){
		JSONObject all = new JSONObject(); // instansiasi berdasarkan JSON sesuai dengan format FCM (lihat pada postman)
		JSONObject notification = new JSONObject(); 
		JSONObject data = new JSONObject();
		
		try {
			data.put("messageId", messages.message_id);// membuat request body sesuai struktur fcm berdasarkan parameter tabel messages
			notification.put("body", messages.body);
			notification.put("title", "PushNotify");
			all.put("to", messages.receiver.token);
			all.put("notification", notification);
			all.put("data", data);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		HttpEntity<String> requestBody = new HttpEntity<String>(all.toString()); // generic class
		//class bawaan java sebagai request body 
		
		RestTemplate restTemplate = new RestTemplate(); // instansiasi untuk request keseluruhan(request body, header, request method)
		
		ArrayList<ClientHttpRequestInterceptor> requestHeader = new ArrayList<ClientHttpRequestInterceptor>(); // instansiasi untuk request Header(Lihat di postman)
		requestHeader.add(new HeaderRequestInterceptor("Authorization","key="+Firebase_server_key));
		requestHeader.add(new HeaderRequestInterceptor("Content-Type","application/json"));
		
		restTemplate.setInterceptors(requestHeader); // request header di set ke rest template (request secara keseluruhan)
		String response = restTemplate.postForObject(Firebase_API, requestBody, String.class); // kirim rest template (request secara keseluruhan) ke firebase 
		
		return CompletableFuture.completedFuture(response);
	}
	
}
