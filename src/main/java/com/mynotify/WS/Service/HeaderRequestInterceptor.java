package com.mynotify.WS.Service;

import java.io.IOException;

import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.support.HttpRequestWrapper;

public class HeaderRequestInterceptor implements ClientHttpRequestInterceptor {
	
	private final String headerName;
	private final String headerValue;
	
	public HeaderRequestInterceptor(String headerName, String headerValue){
		this.headerName = headerName;
		this.headerValue = headerValue;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest arg0, byte[] arg1, ClientHttpRequestExecution arg2)
			throws IOException {
		// TODO Auto-generated method stub
		HttpRequest wrapper = new HttpRequestWrapper(arg0);
		wrapper.getHeaders().set(headerName, headerValue);
		return arg2.execute(wrapper, arg1);
	}

}
