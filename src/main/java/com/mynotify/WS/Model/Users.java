package com.mynotify.WS.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class Users {
	@Id 
	@Column(name = "id")
	public Integer id;
	
	@Column(name = "token")
	public String token;

}
