package com.mynotify.WS.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name= "tmp")
public class Tmp {
	@Id
	@Column(name= "id")
	public Integer id;
	
	@Column(name= "body")
	public String body;
	
	@Column(name= "sender")
	public Integer sender;
	
	@Column(name= "receiver")
	public Integer receiver;
	
}
