package com.mynotify.WS.Model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Autowired;

@Entity
@Table(name= "messages")
public class Messages {
	
	@Id
	@Column(name="message_id")
	public String message_id;
	
	@Autowired
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="sender")
	public Users sender;
	
	@Autowired
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="receiver")
	public Users receiver;
	
	@Column(name="body")
	public String body;
	
	
	@Column(name="sent_date")
	public Long send_date;
	
	@Column(name="receive_date")
	public Long receive_date;
	
	
}
