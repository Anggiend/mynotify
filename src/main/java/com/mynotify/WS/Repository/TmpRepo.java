package com.mynotify.WS.Repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mynotify.WS.Model.Tmp;

public interface TmpRepo extends CrudRepository<Tmp, Long> {
	@Query("Select t From Tmp t Where t.id =?1")
	Tmp FindTmpById(Integer id);
}
