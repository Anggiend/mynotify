package com.mynotify.WS.Repository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mynotify.WS.Model.Users;

public interface UsersRepo extends CrudRepository<Users, Long> {
	@Query("Select u From Users u Where u.id=?1")
	Users FindByUsersId(Integer id);
}
