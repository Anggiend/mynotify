package com.mynotify.WS.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.mynotify.WS.Model.Messages;
import com.mynotify.WS.Model.Tmp;
import com.mynotify.WS.Model.Users;

public interface MessagesRepo extends CrudRepository<Messages, String> {
	@Query("Select m From Messages m Where m.message_id =?1")
	Messages FindMessageId (String message_id);
	
	@Query("Select m From Messages m Where m.receiver=?1")
	List<Messages> FindMessageByReceiver (Users id); // balikannya adalah list messages karna messages yang akan diterimanya banyak. receivernya adalah users lihat pada message (model)
}
