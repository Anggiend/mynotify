package com.mynotify.WS.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mynotify.WS.Model.Messages;
import com.mynotify.WS.Model.Users;
import com.mynotify.WS.Repository.MessagesRepo;
import com.mynotify.WS.Repository.UsersRepo;


@RestController
@RequestMapping(value="/users") // define maping path ke user controller
public class UsersController {
	@Autowired 
	UsersRepo userrepo;
	
	@Autowired
	MessagesRepo messagesrepo;
	
	@CrossOrigin(origins = "*") // mengizinkan Seluruh IP untuk dapat mengakses controller ini
	@RequestMapping(value="/create", method = RequestMethod.POST) // localhost:8080/users/create
	public Users CreateUser(@RequestBody String input){ // membuat function dengan nama Create User parameternya adalah json tetapi dianggap string 
		ObjectMapper mapper = new ObjectMapper(); // mapper merupakan objek hasil instansiasi dari kelas object mapper 
		JsonNode node = null;
		List<Users> listuser = new ArrayList<Users>();
		
		try{
			node = mapper.readTree(input);
		} catch (IOException e){
			e.printStackTrace();
		}
		
		String token = mapper.convertValue(node.get("token"), String.class);
		listuser = (List<Users>) userrepo.findAll();
		int id = listuser.size()+1; 
		Users user = new Users();
		user.id = id;
		user.token = token;
		userrepo.save(user);
		return user;
	}
	
	@CrossOrigin(origins = "*") // mengizinkan Seluruh IP untuk dapat mengakses controller ini
	@RequestMapping(value="/displaymessages/{id}", method = RequestMethod.GET) // localhost:8080/users/create
	public  List<Messages> DisplayMessages (@PathVariable Integer id){ 
	
		Users user =userrepo.FindByUsersId(id); // id yang tadinya integer jadi bertipe Users
		return messagesrepo.FindMessageByReceiver(user); // ambil query dari messages repo dengan parameter id, tapi id nya udah dirubah jadi users lihat aja diatas
	}
		
}
