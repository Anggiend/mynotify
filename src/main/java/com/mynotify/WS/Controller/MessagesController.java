package com.mynotify.WS.Controller;

import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mynotify.WS.Model.Messages;
import com.mynotify.WS.Model.Tmp;
import com.mynotify.WS.Repository.MessagesRepo;
import com.mynotify.WS.Repository.TmpRepo;
import com.mynotify.WS.Repository.UsersRepo;
import com.mynotify.WS.Service.PushNotifService;

@RestController
@RequestMapping(value="/messages")

public class MessagesController {
	@Autowired
	TmpRepo tmprepo;
	@Autowired
	UsersRepo usersrepo;
	@Autowired
	MessagesRepo messagesrepo;
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/send",method = RequestMethod.POST)
	public boolean SendMessage(@RequestBody String input) {
		ObjectMapper mapper = new ObjectMapper(); // mapper merupakan objek hasil instansiasi dari kelas object mapper 
		JsonNode node = null;
		try{
			node = mapper.readTree(input);
		} catch (IOException e){
			e.printStackTrace();
		}
		
		Integer id = mapper.convertValue(node.get("id"), Integer.class); // ambil parameter input yang dibutuhkan 
		Integer thread = mapper.convertValue(node.get("thread"), Integer.class);
		final Tmp tmp = tmprepo.FindTmpById(id);
		
		
		ExecutorService pool = Executors.newFixedThreadPool(thread); // membuat executor untuk mengeksekusi thread sesuai dengan imputan thread
		CountDownLatch latch = new CountDownLatch(thread);// yaitu class yang berfungsi untuk menghitung mundur sesuai dgn jumlah thread
		
		for(int i=0; i < thread ; i++ ){
			Runnable r = new Runnable(){
				public void run(){
					Messages messages = new Messages();
					messages.message_id = UUID.randomUUID().toString(); // membuat id random 
					messages.body = tmp.body;
					messages.sender = usersrepo.FindByUsersId(tmp.sender);
					messages.receiver = usersrepo.FindByUsersId(tmp.receiver);
					messages.send_date = (long) 0;
					messages.receive_date = (long) 0;
					
					messagesrepo.save(messages);
					
					PushNotifService pushnotifservice = new PushNotifService();
					pushnotifservice.send(messages);
					
					Date now = new Date();
					messages.send_date = now.getTime();
					messagesrepo.save(messages);
					
					latch.countDown();//proses mengurangi
				}
			};
			pool.submit(r);
		}
	
		
		try {
			latch.await();
			return true;
		}
		catch (InterruptedException e){
			e.printStackTrace();
			return false;
		}
	}
	
	@CrossOrigin(origins="*")
	@RequestMapping(value="/updatereceiver",method = RequestMethod.PUT)
	public boolean UpdateReceivedate(@RequestBody String input){
		ObjectMapper mapper = new ObjectMapper(); // mapper merupakan objek hasil instansiasi dari kelas object mapper 
		JsonNode node = null;
		
		
		try{
			node = mapper.readTree(input);
		} catch (IOException e){
			e.printStackTrace();
		}
		
		String message_id = mapper.convertValue(node.get("message_id"), String.class);
		Long receive_date = mapper.convertValue(node.get("receive_date"), Long.class);
		
		Messages messages = messagesrepo.FindMessageId(message_id);
		messages.receive_date = receive_date ;
		
		messagesrepo.save(messages);
		
		return true;
		
	}
}
